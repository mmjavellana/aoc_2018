from days import aoc, day
from utils.decorators import time_it

day_number = 25


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.points = [tuple(map(int, line.split(',')))
                       for line in input_data]

    @time_it
    def part1(self, input_data):
        dist = 3
        points = [[p] for p in self.points]
        constellations = []

        while points:
            c = points.pop()

            for p in points:
                if any(distance(i, j) <= dist for i in c for j in p):
                    p += c
                    break
            else:
                constellations.append(c)

        return len(constellations)

    @time_it
    def part2(self, input_data):
        pass


def distance(p1: tuple, p2: tuple) -> int:
    return sum(abs(i - j) for i, j in zip(p1, p2))


if __name__ == "__main__":
    DaySolution(day_number).run()
