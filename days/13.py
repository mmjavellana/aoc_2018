from days import aoc, day

day_number = 13


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.track = {}
        self.carts = []

        cart_line = {"<": "-", ">": "-", "^": "|", "v": "|"}

        for y, line in enumerate(input_data):
            for x, c in enumerate(line):
                self.track[(x, y)] = cart_line.get(c, c)

                if c in cart_line:
                    self.carts.append(((x, y), c, 0))

    def part1(self, input_data):
        t = 0
        carts = self.carts

        while True:
            t += 1
            carts = sorted(carts, key=lambda x: (x[0][1], x[0][0]))

            for i, (coord, cart, turns) in enumerate(carts):
                v = tuple(sum(x) for x in zip(coord, get_velocity(cart)))
                track = self.track[v]
                new_d = new_direction(cart, track)

                if track == "+":
                    new_d = intersection(cart, turns)
                    turns += 1

                if any(c for c in carts if c[0] == v):
                    break

                carts[i] = v, new_d, turns
            else:
                continue

            break

        return t, v

    def part2(self, input_data):
        t = 0
        carts = self.carts

        while True:
            t += 1
            carts = sorted(carts, key=lambda x: (x[0][1], x[0][0]))
            temp_carts = {cart: cart for cart in carts}

            for cart in carts:
                coord, direction, turns = cart
                if cart not in temp_carts:
                    continue

                v = tuple(sum(x) for x in zip(coord, get_velocity(direction)))
                track = self.track[v]
                new_d = new_direction(direction, track)

                if track == "+":
                    new_d = intersection(direction, turns)
                    turns += 1

                temp_carts[cart] = (v, new_d, turns)

                collisions = [k for k, c in temp_carts.items() if c[0] == v]
                if len(collisions) > 1:
                    for k in collisions:
                        del temp_carts[k]

            else:
                carts = list(temp_carts.values())

                if len(carts) == 1:
                    break

                continue

            break

        return t, carts[0]


def get_velocity(argument):
    switcher = {
        ">": (1, 0),
        "^": (0, -1),
        "<": (-1, 0),
        "v": (0, 1)
    }

    return switcher.get(argument, (0, 0))


def new_direction(direction, path):
    switcher = {
        "/<": "v",
        "/^": ">",
        "/>": "^",
        "/v": "<",
        "\\v": ">",
        "\\<": "^",
        "\\^": "<",
        "\\>": "v",
    }

    return switcher.get(path+direction, direction)


def intersection(direction, turns):
    x = "^v<>"
    y = "<>v^"
    turn = turns % 3

    if turn == 0:
        direction = y[x.index(direction)]
    elif turn == 2:
        direction = x[y.index(direction)]

    return direction


if __name__ == "__main__":
    DaySolution(day_number).run()
