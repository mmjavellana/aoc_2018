from days import aoc, day
import re

day_number = 10


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        lines = [[int(i) for i in re.findall(r'-?\d+', l)] for l in input_data]
        self.points = {(x, y): (vx, vy) for (x, y, vx, vy) in lines}

    def part1(self, input_data):
        current_grid = list(self.points.keys())
        bounds = get_bounds(current_grid)
        current_grid_area = get_area(bounds[0], bounds[1])
        t = 0

        while True:
            t += 1
            new_grid = step_grid(self.points, t)
            new_bounds = get_bounds(new_grid)
            grid_area = get_area(new_bounds[0], new_bounds[1])

            if grid_area > current_grid_area:
                break

            current_grid = new_grid
            current_grid_area = grid_area
            bounds = new_bounds

        print_grid(current_grid, bounds)

        return t - 1

    def part2(self, input_data):
        return "See Part 1"


def step_grid(grid: dict, time: int) -> "dict":
    return [(p[0] + v[0] * time, p[1] + v[1] * time)
            for p, v in grid.items()]


def get_bounds(grid: dict) -> "tuple":
    return list(map(min, *grid)), list(map(max, *grid))


def get_area(min_bound: tuple, max_bound: tuple) -> "int":
    return (max_bound[0] - min_bound[0]) + (max_bound[1] - min_bound[1])


def print_grid(grid: dict, bounds: tuple) -> "None":
    print(' ')
    for y in range(bounds[0][1] - 1, bounds[1][1] + 2):
        for x in range(bounds[0][0] - 1, bounds[1][0] + 2):
            print('#' if (x, y) in grid else '.', end='')
        print(' ')
    print(' ')


if __name__ == "__main__":
    DaySolution(day_number).run()
