from days import aoc, day
from collections import defaultdict
import re

day_number = 7


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        p = re.compile(r"Step (\w) .* step (\w)")
        self.parsed_data = [(m[1], m[2])
                            for m in [p.match(i) for i in input_data]]
        self.mapped_data = defaultdict(list)
        [self.mapped_data[r].append(l) for l, r in self.parsed_data]
        [self.mapped_data[l]
         for l, r in self.parsed_data if l not in self.mapped_data]

    def part1(self, input_data):
        s = ""

        while len(s) < len(self.mapped_data):
            s += sorted([i for i, v in self.mapped_data.items()
                        if len([c for c in v if c not in s]) == 0
                        and i not in s])[0]

        return s

    def part2(self, input_data):
        def convert(s): return ord(s.lower()) % 32 + 60

        workers = 5

        s = ""
        q = {i: None for i in range(workers)}
        t = -1

        while len(s) < len(self.mapped_data):
            t += 1
            active = [i for i in q if q[i] is not None]

            q.update({i: (q[i][0], q[i][1] - 1) for i in active})

            for i in (x for x in active if q[x][1] <= 0):
                s += q[i][0]
                q[i] = None

            f = [i for i in q if q[i] is None]

            if f:
                h = [i for i, v in self.mapped_data.items()
                     if len([c for c in v if c not in s]) == 0]
                h = [i for i in h
                     if i not in s and
                     i not in [c[0] for k, c in q.items() if c is not None]]
                q.update({i: (c, convert(c)) for i, c in zip(f, sorted(h))})

        return t


if __name__ == "__main__":
    DaySolution(day_number).run()
