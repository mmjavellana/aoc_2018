from days import aoc, day
from utils.decorators import time_it
from utils import opcodes

day_number = 21


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.op_codes = {
            "addr": opcodes.addr,
            "addi": opcodes.addi,
            "mulr": opcodes.mulr,
            "muli": opcodes.muli,
            "banr": opcodes.banr,
            "bani": opcodes.bani,
            "borr": opcodes.borr,
            "bori": opcodes.bori,
            "setr": opcodes.setr,
            "seti": opcodes.seti,
            "gtir": opcodes.gtir,
            "gtri": opcodes.gtri,
            "gtrr": opcodes.gtrr,
            "eqir": opcodes.eqir,
            "eqri": opcodes.eqri,
            "eqrr": opcodes.eqrr,
            }

        self.ip = int(input_data[0].split()[1])
        self.instructions = [(i[0], *[int(x) for x in i[1:]])
                             for i in [i.split() for i in input_data[1:]]]

    @time_it
    def part1(self, input_data):
        return self.execute_instructions()

    @time_it
    def part2(self, input_data):
        return self.execute_instructions(shortest=False)

    def execute_instructions(self, shortest=True):
        a, ip, b, c, d, e = [0] * 6
        targets = []

        while True:
            d = e | 65536  # 6
            e = 13431073  # 7

            while True:
                c = d & 255  # 8
                e += c  # 9
                e &= 16777215  # 10
                e *= 65899  # 11
                e &= 16777215  # 12

                if 256 > d:
                    if(shortest):
                        return e
                    else:
                        if e in targets:
                            return targets[-1]

                        targets.append(e)
                        break
                else:
                    d //= 256

            if e == a:
                break

        return e


if __name__ == "__main__":
    DaySolution(day_number).run()
