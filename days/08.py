from days import aoc, day

day_number = 8


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.parsed_data = [int(i) for i in input_data[0].split()]
        self.tree, _ = ParseTree(self.parsed_data, 1)

    def part1(self, input_data):
        nodes = [self.tree]
        total = 0

        while(True):
            newNodes = []
            if nodes(nodes):
                break

            for node in nodes:
                total += sum(node.metadata)
                newNodes.extend(node.children)

            nodes = newNodes

        return total

    def part2(self, input_data):
        nodes = [self.tree]
        total = 0

        while(True):
            newNodes = []
            if not(nodes):
                break
            for node in nodes:
                if not(node.children):
                    total += sum(node.metadata)
                else:
                    newNodes.extend([
                        node.children[i - 1]
                        for i in node.metadata
                        if i <= len(node.children)
                    ])

            nodes = newNodes

        return total


class Tree:
    def __init__(self, key):
        self.key = key
        self.children = []
        self.metadata = []

    def add_child(self, child):
        self.children.append(child)

    def set_parent(self, parent):
        self.parent = parent


def ParseTree(data: list, key: int = None) -> "tuple(Tree, list)":
    children, metadata = data[:2]
    data = data[2:]

    tree = Tree(key)

    for i in range(children):
        key += 1
        child, data = ParseTree(data, key)
        tree.add_child(child)

    tree.metadata = data[:metadata]

    return tree, data[metadata:]


if __name__ == "__main__":
    DaySolution(day_number).run()
