from days import aoc, day
from itertools import product
from collections import defaultdict

day_number = 11


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.serial_number = int(input_data[0])
        self.grid = {i: calculate_power(*i, self.serial_number)
                     for i in product(range(1, 301), repeat=2)}
        self.lookup_grid = create_sum_grid(self.grid)

    def part1(self, input_data):
        return get_grid_max(self.lookup_grid, 3)

    def part2(self, input_data):
        max_total = None
        best_size = 1
        start_point = (1, 1)
        for size in range(1, 301):
            point, total = get_grid_max(self.lookup_grid, size)

            if not(max_total) or total > max_total:
                max_total = total
                start_point = point
                best_size = size

        return start_point, best_size, max_total


def calculate_power(x, y, serial_number: int) -> "int":
    return (((((x + 10) * y) + serial_number) * (x + 10)) // 100 % 10) - 5


def get_grid_max(grid: dict, size: int) -> "tuple":
    max_total = None
    start_point = None

    for x, y in (product(range(300 - (size - 1)), repeat=2)):
        total = (grid[x + size][y + size]
                 - grid[x + size][y]
                 - grid[x][y + size]
                 + grid[x][y])

        if not(max_total) or total > max_total:
            max_total = total
            start_point = x + 1, y + 1

    return start_point, max_total


def create_sum_grid(grid: dict) -> "dict":
    bounds = list(map(min, *grid)), list(map(max, *grid))
    sum_grid = defaultdict(lambda: defaultdict(int))

    for y in range(bounds[0][1], bounds[1][1] + 1):
        for x in range(bounds[0][0], bounds[1][0] + 1):
            sum_grid[x][y] = (grid[x, y]
                              + sum_grid[x][y - 1]
                              + sum_grid[x - 1][y]
                              - sum_grid[x - 1][y - 1])

    return sum_grid


if __name__ == "__main__":
    DaySolution(day_number).run()
