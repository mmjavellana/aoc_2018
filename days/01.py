from days import aoc, day

day_number = 1


@day(day_number)
class DaySolution(aoc):
    def part1(self, input_data):
        frequency = 0

        for x in input_data:
            frequency += int(x)

        return frequency

    def part2(self, input_data):
        frequency = 0
        frequencies = set()
        foundDuplicate = False

        loopCounter = 0

        while not(foundDuplicate):
            loopCounter += 1

            for x in input_data:
                frequencies.add(frequency)
                frequency += int(x)

                if frequency in frequencies:
                    foundDuplicate = True
                    break

        # print (f"Looped {loopCounter} times")
        return frequency


if __name__ == "__main__":
    DaySolution(day_number).run()
