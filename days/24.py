from days import aoc, day
from utils.decorators import time_it
import re

day_number = 24


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        pass

    @time_it
    def part1(self, input_data):
        return emulate_combat(parse_armies(input_data))

    @time_it
    def part2(self, input_data):
        target = input_data[0][:-1]
        boost = 0

        while True:
            boost += 1
            armies = [a.boost(boost) if a.army == target else a
                      for a in parse_armies(input_data)]
            winner, score = emulate_combat(armies)

            if winner == target:
                break

        return winner, score, boost


def emulate_combat(armies: list['Unit']):
    while len(set(a.army for a in armies)) > 1:
        c = {}
        units = sum(a.units for a in armies)

        armies.sort(key=lambda a: (a.power, a.initiative), reverse=True)
        for a in armies:
            c[a] = a.choose_target(set(armies).difference(c.values()))

        armies.sort(key=lambda a: a.initiative, reverse=True)
        for a in armies:
            if c[a] and a.power:
                a.attack(c[a])

        if units == sum(a.units for a in armies):
            return None, 0

        armies = [a for a in armies if a.power]
        armies.sort(key=lambda a: a.id)

    return armies[0].army, sum(a.units for a in armies)


def parse_armies(config: str) -> list['Unit']:
    armies = []
    army = None
    count = 0

    for s in config:
        if not s:
            army = None
            continue

        if not army:
            army = s[:-1]
            count = 0
            continue

        count += 1
        armies.append(Unit.parse(army, count, s))

    return armies


class Unit():
    def __init__(self, army, id, units, hp, dmg, type_, mods, init):
        self.__id = id
        self.__army = army
        self.__units = units
        self.__hp = hp
        self.__damage = dmg
        self.__damage_type = type_
        self.__initiative = init
        self.__modifiers = mods

    @property
    def id(self) -> str: return f'{self.__army} Group {self.__id}'

    @property
    def army(self) -> str: return self.__army

    @property
    def units(self) -> int: return self.__units

    @property
    def initiative(self) -> int: return self.__initiative

    @property
    def immune(self) -> list[str]: return self.__modifiers.get('immune', [])

    @property
    def weak(self) -> list[str]: return self.__modifiers.get('weak', [])

    @property
    def power(self) -> int: return self.__damage * self.__units

    def boost(self, boost) -> 'Unit':
        self.__damage += boost
        return self

    def calc_damage(self, target: 'Unit') -> int:
        if self.__damage_type in target.immune:
            return 0

        mod = 2 if self.__damage_type in target.weak else 1
        return self.power * mod

    def choose_target(self, targets: list['Unit']) -> 'Unit':
        options = [(d, t)
                   for t in targets if t.army != self.army
                   if (d := self.calc_damage(t))]
        options.sort(key=lambda t: (t[0], t[1].power, t[1].initiative),
                     reverse=True)
        return next((t for _, t in options), None)

    def attack(self, target: 'Unit') -> None:
        if target:
            target.defend(self.power, self.__damage_type)

    def defend(self, damage: int, type_: str) -> None:
        mod = 2 if type_ in self.weak else 1
        self.__units -= min((damage * mod) // self.__hp, self.__units)

    @staticmethod
    def parse(army: str, id_: int, config: str) -> 'Unit':
        units, hp, damage, init = [int(i) for i in re.findall(r'\d+', config)]
        type_ = re.search(r'\w+(?= damage)', config).group()
        mods = re.search(r'(?<=\().*(?=\))', config)
        if mods:
            mods = [r.split(' to ') for r in mods.group().split('; ')]
            mods = {k: v.split(', ') for k, v in mods}

        return Unit(army, id_, units, hp, damage,
                    type_, mods or {}, init)

    def __str__(self) -> str:
        s = f'{self.id}: {self.__units} units'
        s += f' with {self.__hp} hp each'

        if self.__modifiers.keys():
            s += ' ('
            s += '; '.join([f'{k} to ' + ', '.join(v)
                           for k, v in self.__modifiers.items() if v])
            s += ') '

        s += f' doing {self.__damage} {self.__damage_type} damage'
        s += f' at {self.__initiative} initiative'
        return s

    def __repr__(self) -> str:
        return f'{self.id}: {self.__units} units'


if __name__ == "__main__":
    DaySolution(day_number).run()
