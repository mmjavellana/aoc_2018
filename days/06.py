from days import aoc, day
from itertools import product
from collections import defaultdict

day_number = 6


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.parsed_data = {}

        x0 = y0 = float("inf")
        x1 = y1 = -1

        for i, s in enumerate(input_data):
            c = list(map(int, s.split(",")))

            x0 = c[0] if c[0] < x0 else x0
            x1 = c[0] if c[0] > x1 else x1

            y0 = c[1] if c[1] < y0 else y0
            y1 = c[1] if c[1] > y1 else y1

            self.parsed_data[i] = (c[0], c[1])

        self.small_bounds = (x0, y0)
        self.large_bounds = (x1, y1)

        self.grid = defaultdict(lambda: defaultdict(list))
        for p in product(range(self.small_bounds[0],
                               self.large_bounds[0] + 1),
                         range(self.small_bounds[0],
                               self.large_bounds[1] + 1)):
            for i, c in self.parsed_data.items():
                self.grid[p][getManhattanDistance(p, c)].extend(i)

    def part1(self, input_data):
        grid = {i: v[m][0] if len(v[m]) == 1 else '.'
                for i, v in self.grid.items() for m in [min(v)]}
        inf = set([v for i, v in grid.items()
                  if i[0] == self.small_bounds[0]
                  or i[0] == self.large_bounds[0]
                  or i[1] == self.small_bounds[1]
                  or i[1] == self.large_bounds[1]])

        dist = {i: list(grid.values()).count(i)
                for i, v in self.parsed_data.items() if i not in inf}

        return max(dist.values())

    def part2(self, input_data):
        grid = {i: sum([k * len(c) for k, c in v.items()])
                for i, v in self.grid.items()}

        return len([i for i, v in grid.items() if v < 10000])


def getManhattanDistance(p: tuple, q: tuple) -> "int":
    return abs(p[0] - q[0]) + abs(p[1] - q[1])


if __name__ == "__main__":
    DaySolution(day_number).run()
