from days import aoc, day
from utils.decorators import time_it
from collections import defaultdict
from utils import opcodes
import re

day_number = 16


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.op_codes = [
            opcodes.addr,
            opcodes.addi,
            opcodes.mulr,
            opcodes.muli,
            opcodes.banr,
            opcodes.bani,
            opcodes.borr,
            opcodes.bori,
            opcodes.setr,
            opcodes.seti,
            opcodes.gtir,
            opcodes.gtri,
            opcodes.gtrr,
            opcodes.eqir,
            opcodes.eqri,
            opcodes.eqrr,
            ]

    @time_it
    def part1(self, input_data):
        numbers = re.compile(r'\d')
        total = 0
        for i in range(0, len(input_data), 4):
            if not input_data[i]:
                break

            initial = [int(i) for i in numbers.findall(input_data[i])]
            expected = [int(i) for i in numbers.findall(input_data[i+2])]
            command = [int(i) for i in input_data[i + 1].split()]
            matches = 0

            for op in self.op_codes:
                result = op(*command[1:], initial[:])

                if expected == result:
                    matches += 1

                if matches >= 3:
                    total += 1
                    break

        return total

    @time_it
    def part2(self, input_data):
        numbers = re.compile(r'\d')
        temp_codes = defaultdict(set)
        op_codes = {}

        for i in range(0, len(input_data), 4):
            if not input_data[i]:
                break

            initial = [int(i) for i in numbers.findall(input_data[i])]
            expected = [int(i) for i in numbers.findall(input_data[i+2])]
            command = [int(i) for i in input_data[i + 1].split()]

            for op in self.op_codes:
                result = op(*command[1:], initial[:])

                if expected == result:
                    temp_codes[command[0]].add(op)
                elif op in temp_codes[command[0]]:
                    temp_codes[command[0]].remove(op)

        while temp_codes:
            for code in (k for k, v in temp_codes.items() if len(v) == 1):
                op = temp_codes[code].pop()
                op_codes[code] = op

                for k in (k for k, v in temp_codes.items() if op in v):
                    temp_codes[k].remove(op)

            temp_codes = {k: v for k, v in temp_codes.items() if len(v) > 0}

        registers = [0, 0, 0, 0]

        for i in range(i + 3, len(input_data)):
            input_ = [int(i) for i in input_data[i].split()]
            code = input_[0]
            command = input_[1:]
            registers = op_codes[code](*command, registers)

        print(i)

        return registers


if __name__ == "__main__":
    DaySolution(day_number).run()
