from days import aoc, day
from collections import defaultdict
from itertools import islice
from types import GeneratorType

day_number = 12


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.state = input_data[0].split(': ')[1]
        self.rules = {s[0]: s[1]
                      for s in (l.split(' => ') for l in input_data[2:])
                      if s[1] == '#'}
        self.rules = defaultdict(lambda: '.', self.rules)

    def part1(self, input_data):
        return self.generate_sum(self.get_generation(20))

    def part2(self, input_data):
        goal = 50000000000
        prev_state = self.state

        for i, state in enumerate(self.calculate_generations()):
            if state.strip('.') in prev_state:
                break

            prev_state = state

        prev_sum = self.generate_sum(prev_state)
        diff = self.generate_sum(state) - prev_sum

        return i, prev_sum + diff * (goal - i)

    def get_generation(self, generation):
        return next(islice(self.calculate_generations(),
                    generation - 1, generation))

    def calculate_generations(self) -> "GeneratorType":
        state = self.state

        while True:
            prev_state = f"....{state}...."
            state = ''.join(self.rules[prev_state[i-2:i+3]]
                            for i in range(2, len(prev_state) - 2))

            yield state

    def generate_sum(self, state: str) -> "int":
        diff = (len(state) - len(self.state))//2
        return sum(i - diff for i, c in enumerate(state) if c == '#')


if __name__ == "__main__":
    DaySolution(day_number).run()
