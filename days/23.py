from days import aoc, day
from utils.decorators import time_it
from itertools import product
import math
import heapq
import re

day_number = 23


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.points = {}
        numbers = re.compile(r'-?\d+')

        for line in input_data:
            values = [int(i) for i in numbers.findall(line)]

            self.points[tuple(x for x in values[:-1])] = values[-1]

    @time_it
    def part1(self, input_data):
        strong = max(self.points, key=lambda k: self.points[k])

        return sum(1 for k in self.points.keys()
                   if getManhattanDistance(strong, k) <= self.points[strong])

    @time_it
    def part2(self, input_data):
        max_bounds = tuple(max(a) for a in zip(*self.points))
        min_bounds = tuple(min(a) for a in zip(*self.points))
        size = max(*(p[0] - p[1] for p in zip(max_bounds, min_bounds)))
        center = tuple(p + size for p in min_bounds)
        origin = tuple(0 for i in min_bounds)

        directions = set(product((1, *origin[:-1]), repeat=len(origin)))

        # make the size a power of two for even divisions
        size = int(math.pow(2, math.ceil(math.log(size)/math.log(2))))
        min_bounds = tuple(p - size // 2 for p in center)

        queue = [(-len(self.points),
                  getDistanceFromShape(min_bounds, size, origin),
                  size,
                  min_bounds)]

        seen = []

        while queue:
            bots, dist, size, point = heapq.heappop(queue)
            bots = -bots
            seen.append(point)

            if size == 1:
                break

            new_size = size // 2

            grid_points = [tuple(p + i * new_size for p, i in zip(point, d))
                           for d in directions]

            for p in grid_points:
                bot_count = sum(1 for b, v in self.points.items()
                                if (getDistanceFromShape(p, new_size, b)
                                    <= v))

                if bot_count:
                    heapq.heappush(queue,
                                   (-bot_count,
                                    getDistanceFromShape(p,
                                                         new_size,
                                                         origin),
                                    new_size,
                                    p))

        print(bots, dist, size, point, len(seen))
        return dist


def getManhattanDistance(p1: tuple, p2: tuple) -> "int":
    return sum(abs(i - j) for i, j in zip(p1, p2))


def getDistanceFromShape(center: tuple, size: int, point: tuple) -> "int":
    min_p = tuple(i for i in center)
    max_p = tuple(i + size - 1 for i in center)
    nearest = tuple(max(p[0] - p[2], 0, p[2] - p[1])
                    for p in zip(min_p, max_p, point))
    return sum(nearest)


if __name__ == "__main__":
    DaySolution(day_number).run()
