from days import aoc, day
from typing import Generator
from utils.decorators import time_it

day_number = 14


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.recipes = input_data[0]
        self.scoreboard = [3, 7]
        self.elves = [0, 1]

    def part1(self, input_data):
        goal = 10
        count = int(self.recipes)

        for scoreboard in self.generate_recipes():
            if len(scoreboard) >= count + goal:
                break

        return "".join(str(i)
                       for i in scoreboard[count: count + goal])

    @time_it
    def part2(self, input_data):
        goal = [int(i) for i in self.recipes]
        length = len(goal)

        for scoreboard in self.generate_recipes():
            if scoreboard[-length:] == goal:
                break

        return len(scoreboard) - length

    def generate_recipes(self) -> Generator:
        scoreboard = self.scoreboard.copy()
        elves = self.elves.copy()
        while True:
            total = str(scoreboard[elves[0]] + scoreboard[elves[1]])

            for i in total:
                scoreboard.append(int(i))
                yield scoreboard

            elves = [(1 + i + scoreboard[i]) % len(scoreboard) for i in elves]


if __name__ == "__main__":
    DaySolution(day_number).run()
