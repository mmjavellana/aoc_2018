from days import aoc, day
from datetime import datetime
from itertools import groupby
import re

day_number = 4


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.sorted_data = input_data
        self.sorted_data.sort()

    def part1(self, input_data):
        class Guard:

            def __init__(self, id, sleepMinute):
                self.Id = id
                self.SleepMinute = sleepMinute

        guards = []
        p = re.compile(r".*#(\d+)\s.*")
        for s in self.sorted_data:
            time = datetime.fromisoformat(s[1:s.index("]")])

            if "#" in s:
                m = p.match(s)
                guardId = int(m.group(1))

            elif "asleep" in s:
                guard = (guardId, time)

            elif "wakes" in s:
                for d in range(guard[1].minute, time.minute):
                    guards.append(Guard(guard[0], d))
                guard = None

        guards = sorted(guards, key=lambda item: item.Id)

        guardGroups = [(key, len(list(group)))
                       for key, group
                       in groupby(guards, lambda item: item.Id)]
        guardGroups = sorted(guardGroups, key=lambda item: item[1],
                             reverse=True)

        guardId = guardGroups[0][0]

        guards = sorted(guards, key=lambda item: item.SleepMinute)

        minuteGroups = [(key, len(list(group)))
                        for key, group
                        in groupby([g for g in guards
                                    if g.Id == guardId],
                                   lambda g: g.SleepMinute)]
        minuteGroups = sorted(minuteGroups, key=lambda item: item[1],
                              reverse=True)

        sleepMinute = minuteGroups[0][0]

        # print((guardId, sleepMinute))
        return guardId * sleepMinute

    def part2(self, input_data):
        from collections import defaultdict

        guards = defaultdict(lambda: defaultdict(int))

        p = re.compile(r".*#(\d+)\s.*")
        for s in self.sorted_data:
            time = datetime.fromisoformat(s[1:s.index("]")])
            if "#" in s:
                m = p.match(s)
                guardId = int(m.group(1))

            elif "asleep" in s:
                guard = (guardId, time)

            elif "wakes" in s:
                for d in range(guard[1].minute, time.minute):
                    guards[guard[0]][d] += 1

                guard = None

        gm = dict([(g, max(m.items(), key=lambda i:i[1]))
                   for g, m in guards.items()])

        maxGuard = max(gm.items(), key=lambda i: i[1][1])

        # print(maxGuard)
        return maxGuard[0] * maxGuard[1][0]


if __name__ == "__main__":
    DaySolution(day_number).run()
