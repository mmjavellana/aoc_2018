from days import aoc, day
from utils.decorators import time_it
import heapq

day_number = 22


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.depth = int(input_data[0].split()[1])
        coords = [int(i) for i in input_data[1].split()[1].split(',')]
        self.target = coords[1] + coords[0] * 1j

    @time_it
    def part1(self, input_data):
        caves = self.get_geologic_indexes(self.target, self.target)
        self.draw_caves(caves)

        return sum(caves.values())

    @time_it
    def part2(self, input_data):
        def update_moves(moves_, new_move, parent, dist_):
            if new_move not in moves_ or moves_[new_move][0] > dist_:
                moves_[new_move] = (dist_, parent)
            return moves_

        buffer = 100
        caves = self.get_geologic_indexes(self.target + buffer + buffer * 1j,
                                          self.target)
        self.draw_caves(caves)
        dirs = [1, -1, 1j, -1j]

        queue = [(int(self.target.real + self.target.imag), 0, 0, 0, 1)]

        distances = {}
        moves = {(0, 0, 1): (0, None)}
        target = (int(self.target.imag), int(self.target.real), 1)

        while queue:
            _, dist, x, y, tool = heapq.heappop(queue)
            coord = y + x * 1j

            if (x, y, tool) in distances and distances[x, y, tool] <= dist:
                continue

            distances[x, y, tool] = dist

            if (x, y, tool) == target:
                break

            t = 3 - caves[coord] - tool
            estimate = abs(target[0] - x) + abs(target[1] - y)
            heapq.heappush(queue, (estimate + dist + 7,
                                   dist + 7, x, y, t))
            moves = update_moves(moves, (x, y, t),
                                 (x, y, tool), dist + 7)

            for p in dirs:
                new_coords = (coord + p, tool)
                nx = int(new_coords[0].imag)
                ny = int(new_coords[0].real)

                if new_coords[0] not in caves:
                    continue

                if caves[new_coords[0]] == tool:
                    continue

                estimate = abs(target[0] - nx) + abs(target[1] - ny)
                heapq.heappush(queue, (estimate + dist + 1,
                                       dist + 1, nx, ny, tool))
                moves = update_moves(moves, (nx, ny, tool),
                                     (x, y, tool), dist + 1)

        path = [(target[0], target[1])]
        x = moves[target]
        while(x[1] in moves):
            path.append((x[1][0], x[1][1]))
            x = moves[x[1]]

        path = path[::-1]
        bounds = tuple(map(max, *path))

        with open(self.output_filename, 'w+') as f:
            for y in range(0, bounds[1] + 1):
                path_line = line = ''
                for x in range(0, bounds[0] + 1):
                    pos = y + x*1j
                    cell = caves[pos] % 3

                    if (x, y) == (0, 0):
                        line += 'M'
                    elif pos == self.target:
                        line += 'T'
                    elif cell == 0:
                        line += '.'
                    elif cell == 1:
                        line += '='
                    elif cell == 2:
                        line += '|'

                    path_line += 'X' if (x, y) in path else line[-1]

                f.write(path_line + (' ' * 10) + line + '\n')

        return distances[target]

    def get_geologic_indexes(self, coords: complex, target: complex):
        max_x = int(coords.imag)
        max_y = int(coords.real)
        indexes = {0+0j: 0, target: 0}
        erosion = {}
        caves = {}

        for y in range(0, max_y + 1):
            for x in range(0, max_x + 1):
                pos = y + x * 1j

                if pos in indexes:
                    pass
                elif y == 0:
                    indexes[pos] = x * 16807
                elif x == 0:
                    indexes[pos] = y * 48271
                else:
                    indexes[pos] = erosion[pos - 1] * erosion[pos - 1j]

                erosion[pos] = (indexes[pos] + self.depth) % 20183
                caves[pos] = erosion[pos] % 3

        # return indexes, erosion, caves
        return caves

    def draw_caves(self, caves):
        coords = [(int(i.imag), int(i.real)) for i in caves]
        bounds = tuple(map(max, *coords))

        with open(self.output_filename, 'w+') as f:
            for y in range(0, bounds[1] + 1):
                line = ''
                for x in range(0, bounds[0] + 1):
                    pos = y + x*1j
                    cell = caves[pos] % 3

                    if (x, y) == (0, 0):
                        line += 'M'
                    elif pos == self.target:
                        line += 'T'
                    elif cell == 0:
                        line += '.'
                    elif cell == 1:
                        line += '='
                    elif cell == 2:
                        line += '|'

                f.write(line + '\n')


if __name__ == "__main__":
    DaySolution(day_number).run()
