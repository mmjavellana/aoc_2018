from days import aoc, day
from collections import defaultdict

day_number = 2


@day(day_number)
class DaySolution(aoc):
    def part1(self, input_data):
        doubles = 0
        triples = 0

        for x in input_data:
            count = defaultdict(int)

            for c in x:
                count[c] += 1

            doubles += 1 if 2 in count.values() else 0
            triples += 1 if 3 in count.values() else 0

        # print(f"Doubles: {doubles}")
        # print(f"Triples: {triples}")
        return doubles * triples

    def part2(self, input_data):
        from itertools import product
        for (x, y) in product(input_data, repeat=2):
            if x == y:
                continue

            differences = 0
            newId = ""

            for a, b in zip(x, y):
                if a == b:
                    newId += a
                else:
                    differences += 1

            if differences == 1:
                break

        # print(f"First id:  {x}")
        # print(f"Second id: {y}")
        # return f"New id:    {newId}"
        return newId


if __name__ == "__main__":
    DaySolution(day_number).run()
