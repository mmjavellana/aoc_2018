from days import aoc, day
from utils.decorators import time_it
from collections import deque

day_number = 20


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.data = input_data[0][1:-1]
        self.world, self.distances = self.build_map(self.data)

    @time_it
    def part1(self, input_data):
        return max(self.distances.values())

    @time_it
    def part2(self, input_data):
        return sum(i >= 1000 for i in self.distances.values())

    def build_map(self, data):
        pos = 0
        map_ = {pos: 'X'}
        distances = {pos: 0}
        dirs = {'N': -1, 'S': 1, 'W': -1j, 'E': 1j}
        stack = deque()

        for c in data:
            if c == '(':
                stack.append(pos)
            elif c in '|':
                pos = stack[-1]
            elif c in ')':
                pos = stack.pop()
            else:
                prev = pos
                ap = dirs[c]
                map_[pos + ap] = '|' if type(ap) is complex else '-'
                pos += 2*ap
                map_[pos] = '.'

                if pos not in distances:
                    distances[pos] = distances[prev] + 1

        coords = [(int(i.imag), int(i.real)) for i in map_]
        bounds = tuple(map(min, *coords)), tuple(map(max, *coords))

        with open(self.output_filename, 'w+') as f:
            for y in range(bounds[0][1] - 1, bounds[1][1] + 2):
                line = ''
                for x in range(bounds[0][0] - 1, bounds[1][0] + 2):
                    line += map_[y + x*1j] if (y + x*1j) in map_ else '#'

                f.write(line + '\n')

        return map_, distances


if __name__ == "__main__":
    DaySolution(day_number).run()
