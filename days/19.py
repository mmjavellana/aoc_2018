from days import aoc, day
from utils.decorators import time_it
from utils import opcodes
from functools import reduce

day_number = 19


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.op_codes = {
            "addr": opcodes.addr,
            "addi": opcodes.addi,
            "mulr": opcodes.mulr,
            "muli": opcodes.muli,
            "banr": opcodes.banr,
            "bani": opcodes.bani,
            "borr": opcodes.borr,
            "bori": opcodes.bori,
            "setr": opcodes.setr,
            "seti": opcodes.seti,
            "gtir": opcodes.gtir,
            "gtri": opcodes.gtri,
            "gtrr": opcodes.gtrr,
            "eqir": opcodes.eqir,
            "eqri": opcodes.eqri,
            "eqrr": opcodes.eqrr,
            }

        self.ip = int(input_data[0].split()[1])
        self.instructions = [(i[0], *[int(x) for x in i[1:]])
                             for i in [i.split() for i in input_data[1:]]]

    @time_it
    def part1(self, input_data):
        registers = [0] * 6
        return self.execute_instructions(registers)

    @time_it
    def part2(self, input_data):
        registers = [0] * 6
        registers[0] = 1
        return self.execute_instructions(registers)

    def execute_instructions(self, registers):
        i = registers[self.ip]

        with open(self.output_filename, 'w+') as f:
            while i < len(self.instructions):
                registers[self.ip] = i

                inst = self.instructions[i]
                registers = self.op_codes[inst[0]](*inst[1:], registers)

                if i == 1:
                    break
                i = registers[self.ip] + 1
                f.write(str(registers) + '\n')

        target = max(registers)
        factors = set(reduce(list.__add__, [[i, target//i]
                             for i in range(1, int(target**0.5) + 1)
                             if not target % i]))

        return sum(factors)


if __name__ == "__main__":
    DaySolution(day_number).run()
