from days import aoc, day
from collections import defaultdict
from itertools import product

day_number = 3


class Claim:
    def __init__(self, claim):
        s = claim \
            .replace("@ ", "") \
            .replace("#", "") \
            .replace(":", "") \
            .split(" ")

        self.Id = int(s[0])
        self.Start = tuple(map(int, s[1].split(",")))
        self.Dimensions = tuple(map(int, s[2].split("x")))


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.claims = [Claim(x) for x in input_data]

    def part1(self, input_data):
        cloth = defaultdict(lambda: defaultdict(int))

        for claim in self.claims:
            (x, y) = claim.Start
            (w, h) = claim.Dimensions

            for (a, b) in product(range(x, x + w), range(y, y + h)):
                cloth[a][b] += 1

        total = sum(1 for s in cloth.values() for x in s.values() if x > 1)

        return total

    def part2(self, input_data):
        cloth = defaultdict(lambda: defaultdict(int))

        for claim in self.claims:
            (x, y) = claim.Start
            (w, h) = claim.Dimensions

            for (a, b) in product(range(x, x + w), range(y, y + h)):
                cloth[a][b] += 1

        for claim in self.claims:
            (x, y) = claim.Start
            (w, h) = claim.Dimensions

            overlap = False

            for (a, b) in product(range(x, x + w), range(y, y + h)):
                if cloth[a][b] > 1:
                    overlap = True
                    break

            if not(overlap):
                break

        return claim.Id


if __name__ == "__main__":
    DaySolution(day_number).run()
