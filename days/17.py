from days import aoc, day
from utils.decorators import time_it
from collections import defaultdict, deque
import re

day_number = 17


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.map = defaultdict(lambda: '.')
        numbers = re.compile(r'\d+')

        for line in input_data:
            axis = line[0]
            values = [int(i) for i in numbers.findall(line)]

            i = values[0]

            for j in range(values[1], values[2] + 1):
                coord = (i, j) if axis == 'x' else (j, i)
                self.map[coord] = '#'

        self.bounds = tuple(map(min, *self.map)), tuple(map(max, *self.map))

        self.map[500, 0] = '+'

    @time_it
    def part1(self, input_data):
        map_ = self.map.copy()

        self.write_map(map_)

        map_ = self.fill_map(map_)

        self.write_map(map_)

        return len([v for k, v in map_.items()
                    if v in '|~' and k[1] >= self.bounds[0][1]])

    @time_it
    def part2(self, input_data):
        map_ = self.map.copy()

        self.write_map(map_)

        map_ = self.fill_map(map_)

        self.write_map(map_)

        return len([v for k, v in map_.items()
                    if v in '~' and k[1] >= self.bounds[0][1]])

    def fill_map(self, map_, start=(500, 0)):
        sources = deque([start])
        max_y = self.bounds[1][1]

        while sources:
            source = sources.popleft()
            x, y = source
            if map_[source] == '~':
                continue

            y += 1
            while y <= max_y:
                cell = map_[x, y]
                if cell == '.':
                    map_[x, y] = '|'
                    y += 1
                elif cell in '~#':
                    y -= 1

                    left = x
                    while (map_[left - 1, y] != '#'
                           and map_[left, y + 1] in '#~'):
                        left -= 1

                    right = x
                    while (map_[right + 1, y] != '#'
                           and map_[right, y + 1] in '#~'):
                        right += 1

                    fill = '|'
                    if map_[left - 1, y] == '#' and map_[right + 1, y] == '#':
                        fill = '~'
                    else:
                        if map_[left, y + 1] == '.':
                            sources.append((left, y))

                        if map_[right, y + 1] == '.':
                            sources.append((right, y))

                    for ax in range(left, right + 1):
                        map_[ax, y] = fill
                elif cell == '|':
                    break

        return map_

    def write_map(self, map_):
        with open(self.output_filename, 'w+') as f:
            for y in range(0, self.bounds[1][1] + 1):
                line = ''
                for x in range(self.bounds[0][0] - 1, self.bounds[1][0] + 2):
                    line += map_[x, y]

                f.write(line + '\n')


if __name__ == "__main__":
    DaySolution(day_number).run()
