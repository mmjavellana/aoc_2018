from days import aoc, day
from utils.decorators import time_it
from itertools import product

day_number = 18


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.world = {}

        for y, line in enumerate(input_data):
            for x, cell in enumerate(line.strip()):
                self.world[x, y] = cell

        self.bounds = (x, y)

        self.neighbours = [i for i in product((-1, 0, 1), repeat=2)
                           if i != (0, 0)]

    @time_it
    def part1(self, input_data):
        self.write_world(self.world)
        world = self.take_turns(self.world, 10)

        resources = [v for v in world.values()]

        return resources.count('#') * resources.count('|')

    @time_it
    def part2(self, input_data):
        self.write_world(self.world)
        world = self.take_turns(self.world, 1000000000)

        resources = [v for v in world.values()]

        return resources.count('#') * resources.count('|')

    def take_turns(self, world, turns):
        world = world.copy()
        states = []
        cycle = []

        i = 0
        while i < turns:
            world = self.update_world(world)
            i += 1

            if world in states:
                cycle = states[states.index(world, 1):]
                break
            else:
                states.append(world)

            # self.write_world(world)
            # sleep(.1)

        if i < turns:
            world = cycle[(turns - i) % len(cycle)]

        self.write_world(world)
        return world

    def update_world(self, world):
        new_world = world.copy()

        for x, y in world:
            neighbours = [
                    world[ax, ay] for ax, ay in
                    [(x + ax, y + ay) for ax, ay in self.neighbours]
                    if (ax, ay) in world
                ]
            cell = world[x, y]

            if cell == '.':
                cell = '|' if neighbours.count('|') >= 3 else cell
            elif cell == '|':
                cell = '#' if neighbours.count('#') >= 3 else cell
            elif cell == '#':
                cell = '#' if (neighbours.count('#') >= 1
                               and neighbours.count('|') >= 1) else '.'

            new_world[x, y] = cell

        return new_world

    def write_world(self, world):
        with open(self.output_filename, 'w+') as f:
            for y in range(0, self.bounds[1] + 1):
                line = ''
                for x in range(0, self.bounds[0] + 1):
                    line += world[x, y]

                f.write(line + '\n')


if __name__ == "__main__":
    DaySolution(day_number).run()
