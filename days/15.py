from days import aoc, day
from utils.decorators import time_it
from collections import deque

day_number = 15


@day(day_number)
class DaySolution(aoc):
    @time_it
    def common(self, input_data):
        self.bounds = len(input_data[0]), len(input_data)
        self.map = {(x, y): char
                    for y, line in enumerate(input_data)
                    for x, char in enumerate(line)
                    if char != '#'}

        self.enemies = {'E': 'G', 'G': 'E'}
        self.reading_order = [(0, -1), (-1, 0), (1, 0), (0, 1)]

    @time_it
    def part1(self, input_data):
        world = self.map.copy()

        players = [[k, v, 3, 200]
                   for k, v in self.map.items()
                   if v != '.']
        players = sorted(players, key=lambda x: x[0][::-1])
        turn = 0

        while True:
            players, world, completed = self.take_turn(players, world)

            if not(completed):
                break

            turn += 1

        health = sum(p[3] for p in players if p[3] > 0)

        return turn, health, turn * health

    @time_it
    def part2(self, input_data):
        damage = 3

        while True:
            world = self.map.copy()

            elves = [[k, v, damage, 200]
                     for k, v in self.map.items() if v == 'E']
            goblins = [[k, v, 3, 200] for k, v in self.map.items() if v == 'G']
            elf_count = len(elves)
            goblins_count = len(goblins)
            new_elf_count = elf_count

            players = goblins + elves
            players = sorted(players, key=lambda x: x[0][::-1])
            turn = 0

            while elf_count == new_elf_count:
                players, world, completed = self.take_turn(players, world)
                new_elf_count = len([p for p in players if p[1] == 'E'])

                if not(completed):
                    break

                turn += 1

            goblins_count = len([p for p in players if p[1] == 'G'])

            if elf_count == new_elf_count and goblins_count == 0:
                break

            damage += 1

        health = sum(p[3] for p in players if p[3] > 0)

        return damage, turn, health, turn * health

    def print_world(self, world):
        for y in range(self.bounds[1]):
            for x in range(self.bounds[0]):
                if (x, y) in world:
                    print(world[x, y], end="")
                else:
                    print('#', end="")
            print('')

    def take_turn(self, players, world):
        completed = False

        for i, (coord, kind, damage, health) in enumerate(players):
            if health <= 0:
                continue

            all_enemies = [p[0] for p in players
                           if p[1] == self.enemies[kind]]

            if not(all_enemies):
                break

            in_range = [(p[0] + x, p[1] + y)
                        for p in all_enemies
                        for x, y in self.reading_order]

            if coord not in in_range:
                dest = self.breadth_first_search(coord, in_range, world)

                if not(dest):
                    continue

                if dest and dest != coord:
                    world[coord] = '.'
                    world[dest] = kind
                    coord = dest
                    players[i][0] = dest

            if coord in in_range:
                min_hit_score = 201
                enemy = None

                for x, y in self.reading_order:
                    pos = (coord[0] + x, coord[1] + y)
                    for j, target in enumerate(players):
                        if (target[0] == pos
                                and target[3] > 0
                                and target[3] < min_hit_score
                                and target[1] != kind):
                            min_hit_score = target[3]
                            enemy = j

                if enemy is not None:
                    players[enemy][3] -= damage

                    if players[enemy][3] <= 0:
                        world[players[enemy][0]] = '.'
                        players[enemy][1] = 'X'

        else:
            completed = True

        players = [p for p in players if p[3] > 0]
        players = sorted(players, key=lambda x: x[0][::-1])

        return players, world, completed

    def breadth_first_search(self, start, targets, world):
        stack = deque([(start, 0)])
        moves = {start: (0, None)}
        seen = set()
        occupied = [k for k, v in world.items() if v != '.']

        while stack:
            coord, dist = stack.popleft()

            for ax, ay in self.reading_order:
                new_coords = (coord[0] + ax, coord[1] + ay)

                if new_coords not in world:
                    continue
                if start == new_coords or new_coords in occupied:
                    continue

                if (new_coords not in moves
                        or (moves[new_coords][0] > dist+1
                            and moves[new_coords][1][::-1] > coord[::-1])):
                    moves[new_coords] = (dist+1, coord)

                if new_coords in seen:
                    continue

                if not any(new_coords == s[0] for s in stack):
                    stack.append((new_coords, dist+1))

            seen.add(coord)

        try:
            _, closest = min(((dist, pos) for pos, (dist, parent) in
                             moves.items() if pos in targets),
                             key=lambda x: (x[0], x[1][::-1]))
        except ValueError:
            return None

        while moves[closest][0] > 1:
            closest = moves[closest][1]

        return closest


if __name__ == "__main__":
    DaySolution(day_number).run()
