from days import aoc, day
import re

day_number = 5


@day(day_number)
class DaySolution(aoc):
    def common(self, input_data):
        self.input_data = input_data[0]

    def part1(self, input_data):
        return len(CollapseString(self.input_data))

    def part2(self, input_data):
        length = len(self.input_data)

        for c in char_range('a', 'z'):
            s = re.sub(f"[{c}]", '', self.input_data, flags=re.IGNORECASE)
            current_length = len(CollapseString(s))
            length = min(current_length, length)

        return length


def CollapseString(s: str) -> str:
    def to_lower(m): return "[" + m.group(1).lower() + "]"

    def to_upper(m): return m.group(1).upper()

    s = re.sub("([A-Z])", to_lower, s)

    p = re.compile(r'(?:\[([a-z])\]\1|([a-z])\[\2\])')

    while(p.search(s)):
        s = p.sub('', s)

    s = re.sub(r"\[([a-z])\]", to_upper, s)

    return s


def char_range(c1: chr, c2: chr) -> "list(chr)":
    for c in range(ord(c1), ord(c2) + 1):
        yield chr(c)


if __name__ == "__main__":
    DaySolution(day_number).run()
