from days import aoc, day
from collections import deque
import re

day_number = 9


@day(day_number)
class DaySolution(aoc):
    num_players = 0
    num_marbles = 0

    def common(self, input_data):
        p = re.compile(r'([\d]*) players;.* ([\d]*) points')
        m = p.match(self.input_data[0])

        if m:
            self.num_players = int(m.group(1))
            self.max_marbles = int(m.group(2))

    def part1(self, input_data):
        players, _ = play_game(self.num_players, self.max_marbles)
        return max(players.values())

    def part2(self, input_data):
        players, _ = play_game(self.num_players, self.max_marbles * 100)
        return max(players.values())


def play_game(num_players: int, max_marbles: int) -> "(dict(), deque())":
    players = {i: 0 for i in range(num_players)}
    current = 0
    game = deque([0])

    for i in range(1, max_marbles + 1):
        if i % 23 == 0:
            game.rotate(7)
            players[current] += i + game.pop()
            game.rotate(-1)
        else:
            game.rotate(-1)
            game.append(i)

        current = (current + 1) % num_players

    return players, game


if __name__ == "__main__":
    DaySolution(day_number).run()
