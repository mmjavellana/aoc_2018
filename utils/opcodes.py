def addr(a, b, c, registers):
    registers[c] = registers[a] + registers[b]
    return registers


def addi(a, b, c, registers):
    registers[c] = registers[a] + b
    return registers


def mulr(a, b, c, registers):
    registers[c] = registers[a] * registers[b]
    return registers


def muli(a, b, c, registers):
    registers[c] = registers[a] * b
    return registers


def banr(a, b, c, registers):
    registers[c] = registers[a] & registers[b]
    return registers


def bani(a, b, c, registers):
    registers[c] = registers[a] & b
    return registers


def borr(a, b, c, registers):
    registers[c] = registers[a] | registers[b]
    return registers


def bori(a, b, c, registers):
    registers[c] = registers[a] | b
    return registers


def setr(a, b, c, registers):
    registers[c] = registers[a]
    return registers


def seti(a, b, c, registers):
    registers[c] = a
    return registers


def gtir(a, b, c, registers):
    registers[c] = 1 if a > registers[b] else 0
    return registers


def gtri(a, b, c, registers):
    registers[c] = 1 if registers[a] > b else 0
    return registers


def gtrr(a, b, c, registers):
    registers[c] = 1 if registers[a] > registers[b] else 0
    return registers


def eqir(a, b, c, registers):
    registers[c] = 1 if a == registers[b] else 0
    return registers


def eqri(a, b, c, registers):
    registers[c] = 1 if registers[a] == b else 0
    return registers


def eqrr(a, b, c, registers):
    registers[c] = 1 if registers[a] == registers[b] else 0
    return registers
